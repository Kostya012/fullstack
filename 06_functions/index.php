<?php
session_start();
// 1. Вам нужно создать массив и заполнить его случайными числами от 1 до 100 (ф-я rand). Далее, вычислить произведение тех элементов,
//которые больше нуля и у которых четные индексы. После вывести на экран элементы, которые больше нуля и у которых нечетный индекс.
echo 'Task 1<br>';

/**
 * @param int $min
 * @param int $max
 *
 * @return array
 */
function createRandNum(int $min, int $max):array
{
  $arr = array();
  for ($i = 0; $i < 100; $i++) {
    $arr[$i] = rand($min, $max);
  }
  return $arr;
}

$min = 1;
$max = 100;
$arr = createRandNum($min, $max);


$mult = 1;
$arrNotEven = array();
foreach ($arr as $key => $value) {
  if ($value > 0) {
    if ($key % 2 == 0) {
      $mult *= $value;
    } else {
      $arrNotEven[] = $value;
    }
  }
}
echo 'Произведение тех элементов, которые больше нуля и у которых четные индексы= ' . $mult;
echo '<br>';

/**
 * @param array $arr
 * @return bool
 */
function showNotEvenNum(array $arr):bool
{
  echo 'Элементы, которые больше нуля и у которых нечётный индекс:<br>';
  foreach ($arr as $value) {
    echo $value . ' ';
  }
  return true;
}

showNotEvenNum($arrNotEven);

echo '<hr>';
// 2 Даны два числа. Найти их сумму и произведение. Даны два числа. Найдите сумму их квадратов.
echo 'Task 2<br>';

/**
 * @param int $a
 * @param int $b
 *
 * @return int
 */
function sum(int $a, int $b):int
{
  return $a + $b;
}

/**
 * @param int $min
 * @param int $max
 * @return int
 */
function createNum(int $min = 1, int $max = 15):int
{
  return rand($min, $max);
}

/**
 * @param int $a
 * @param int $b
 * @return int
 */
function multiplication(int $a, int $b):int
{
  return $a * $b;
}

/**
 * @param int $a
 * @param int $b
 * @return int
 */
function powSum(int $a, int $b):int
{
  return pow($a, 2) + pow($b, 2);
}

$a = createNum();
$b = createNum();;
echo $a + $b . ' = ' . sum($a, $b) . '<br>';
echo $a * $b . ' = ' . multiplication($a, $b) . '<br>';
echo $a^2 + $b^2 . ' = ' . powSum($a, $b) . '<br>';

echo '<hr>';
// 3. Даны три числа. Найдите их среднее арифметическое.
echo 'Task 3<br>';

/**
 * @param int ...$args
 * @return int
 */
function middleSum(int ...$args):int
{
  $count = count($args);
  $sum = 0;
  foreach ($args as $val) {
    $sum += $val;
  }
  return $sum / $count;
}

$a = createNum();
$b = createNum();
$c = createNum();

echo 'Среднее число ' . $a . ', ' . $b . ', ' . $c . ' = ' . middleSum($a, $b, $c);
echo '<br>';

echo '<hr>';

// 4. Дано число. Увеличьте его на 30%, на 120%.
echo 'Task 4<br>';

/**
 * @param int $num
 * @param int $percent
 * @return int
 */
function percentageIncrease(int $num, int $percent):int
{
  $perc = ($percent + 100) / 100;
  return $num * $perc;
}

$num = createNum(1, 100);
echo 'Увеличение числа ' . $num . ' на 30% = ' . percentageIncrease($num, 30);
echo '<br>';
echo 'Увеличение числа ' . $num . ' на 120% = ' . percentageIncrease($num, 120);
echo '<br>';
echo '<hr>';

// 5 Пользователь выбирает из выпадающего списка страну (Турция, Египет или Италия), вводит количество дней для отдыха и указывает,
//есть ли у него скидка (чекбокс). Вывести стоимость отдыха, которая вычисляется как произведение количества дней на 400. Далее это
//число увеличивается на 10%, если выбран Египет, и на 12%, если выбрана Италия. И далее это число уменьшается на 5%, если указана
//скидка.
echo 'Task 5<br>';

/**
 * @param string $value
 * @return string
 */
function clean(string $value = ""):string
{
  $value = strip_tags($value);
  $value = htmlspecialchars($value);
  return $value;
}

/**
 * @return string
 */
function gen_token():string
{
  $token = md5(microtime() . 'salt' . time());
  return $token;
}

/**
 * @param string $token
 * @return bool
 */
function checkToken(string $token):bool
{
  if ($token === $_SESSION["token"]) {
    return true;
  } else {
    return false;
  }
}

if (isset($_POST['send'])) {
  $token = clean($_POST['token']);
  if (checkToken($token)) {
    $country = clean($_POST['country']);
    $count = clean($_POST['count']);
    $discount = clean($_POST['discount']);

    $priceDay = 400;
    $precForEgiptCheck2 = 10;
    $precForItalyCheck3 = 12;
    $discountNum = 0.95;
    $res = multiplication($count, $priceDay);
    if ($country == '2') {
      $res = percentageIncrease($res, $precForEgiptCheck2);
    } elseif ($country == '3') {
      $res = percentageIncrease($res, $precForItalyCheck3);
    }
    if ($discount) {
      $res = $res * $discountNum;
    }
  } else {
    echo 'token error';
    exit();
  }
}

// 6. Пользователь вводит своё имя, пароль, email. Если вся информация указана, то показать эти данные после фразы 'Регистрация прошла
//успешно', иначе сообщить какое из полей оказалось не заполненным.
//$error = false;
if (isset($_POST['register'])) {
  $token = clean($_POST['token']);
  if (checkToken($token)) {
    $userName = clean($_POST['userName']);
    $password = clean($_POST['password']);
    $email = clean($_POST['email']);
    $_SESSION['userName'] = $userName;
    $_SESSION['password'] = $password;
    $_SESSION['email'] = $email;
    $error_name = '';
    $error_password = '';
    $error_email = '';
    $error = false;
    if (strlen($userName) == 0) {
      $error_name = 'Введите имя';
      $error = true;
    }
    if (strlen($password) < 1 || strlen($password) > 11) {
      $error_password = 'Введите пароль от 2 до 11 символов';
      $error = true;
    }
    if ($email == '' || !preg_match('/@/', $email)) {
      $error_email = 'Введите корректный email';
      $error = true;
    }
  } else {
    echo 'token error';
    exit();
  }
}
if (!isset($_POST['register']) && !isset($_POST['send'])) {
  $_SESSION['token'] = gen_token();
}

// 7. Выведите на экран n раз фразу "Silence is golden". Число n вводит пользователь на форме. Если n некорректно, вывести фразу "Bad n".

$str = '';
$str_error = '';
$err = false;

if (isset($_POST['show'])) {
  $num = clean($_POST['num']);
  if (is_numeric($num)) {
    $num = (int)$num;
    if ($num > 0 && $num <= 100) {
      for ($i = 1; $i <= $num; $i++) {
        $str .= 'Silence is golden' . '<br>';
      }
    } else {
      $err = true;
      $str_error = 'Bad n';
    }
  } else {
    $err = true;
    $str_error = 'Bad n';
  }
}

?>
  <!doctype html>
  <html lang="en-ru">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Traveling</title>
    <style>
        div {
            margin-bottom: 10px;
            position: relative;
        }

        input[type="number"] {
            width: 100px;
        }

        input[type="number"] + span {
            padding-right: 30px;
        }

        input[type="number"]:invalid + span:after {
            position: absolute;
            content: '✖';
            padding-left: 5px;
            color: #8b0000;
        }

        input[type="number"]:valid + span:after {
            position: absolute;
            content: '✓';
            padding-left: 5px;
            color: #009000;
        }
    </style>
  </head>
  <body>
  <h1>Where do you want relax?</h1>
  <form name="checkout" action="<?php echo $_SERVER['SCRIPT_NAME']; ?>" method="post">
    <input type="hidden" name="token" value="<?= $_SESSION['token']; ?>">
    <div class="form-group">
      <label for="country">Сhoose a country:</label><br>
      <select name="country" size="1">
        <option value=1 selected>Турция</option>
        <option value=2>Египет</option>
        <option value=3>Италия</option>
      </select>
    </div>
    <div class="form-group">
      <label for="count">Enter the number of days:</label><br>
      <input type="number" name="count" id="count" min="1" max="100" value=1>
      <span class="validity"></span>
      <span>(0..9)</span><br>
    </div>
    <div class="form-group">
      <input type="checkbox" name="discount" id="discount">
      <label for="discount">I have discount</label>
    </div>
    <input type=submit name="send" value="Done">
    <input type="reset">
  </form>
  <h2><?php echo 'Стоимость отдыха = ' . $res; ?></h2>
  <hr>
  <p>Task 6</p>
  <h1>Registration</h1>
  <form name="registration" action="<?php echo $_SERVER['SCRIPT_NAME']; ?>" method="post">
    <input type="hidden" name="token" value="<?= $_SESSION['token']; ?>">
    <div class="form-group">
      <label for="userName">Enter your name:</label><br>
      <input type="text" name="userName" id="userName" placeholder="Enter your name" required minlength="2"
             maxlength="11">
      <span class="validity"></span>
      <span style="color:red"><?= $error_name ?></span>
      <span>(A..z, 0..9, min 2 chars)</span><br>
    </div>
    <div class="form-group">
      <label for="password">Enter password:</label><br>
      <input type="password" name="password" id="password" placeholder="Enter password:" required minlength="2"
             maxlength="11">
      <span class="validity"></span>
      <span style="color:red"><?= $error_password ?></span>
      <span>(A..z, 0..9, min 2 chars)</span><br>
    </div>
    <div class="form-group">
      <label for="email">Enter your email:</label><br>
      <input type="email" name="email" id="email" placeholder="Enter your email:" required size="32" minlength="3"
             maxlength="64">
      <span class="validity"></span>
      <span style="color:red"><?= $error_email ?></span>
      <span>(A..z, 0..9, min 2 chars)</span><br>
    </div>
    <input type=submit name="register" value="Register">
    <input type="reset">
  </form>
  <?php
  if (isset($error) && $error == false) {
    echo '<h2>Регистрация прошла успешно!</h2>';
    echo 'Ваше имя: ' . $userName . '<br>';
    echo 'Ваш пароль: ' . $password . '<br>';
    echo 'Ваша почта: ' . $email . '<br>';
  }
  ?>
  <hr>
  <p>Task 7</p>
  <form name="showMessage" action="<?php echo $_SERVER['SCRIPT_NAME']; ?>" method="post">
    <div class="form-group">
      <label for="num">Enter the number of times the message was shown:</label><br>
      <input type="number" name="num" id="count" min="1" max="100" value=1>
      <span class="validity"></span>
      <span>(0..9)</span><br>
    </div>
    <input type=submit name="show" value="Show">
    <input type="reset">
  </form>
  <?php
  if ($err) {
    echo $str_error;
  } else {
    echo $str;
  }
  ?>
  </body>
  </html>
<?php
echo '<hr>';
// 8. Заполнить массив длины n нулями и единицами, при этом данные значения чередуются, начиная с нуля.
echo 'Task 8<br>';
$n = createNum(1, 10);
$arr = array();
for ($i = 0; $i < $n; $i++) {
  if ($i % 2 == 0) {
    $arr[$i] = 0;
  } else {
    $arr[$i] = 1;
  }
}
echo '<pre>';
print_r($arr);
echo '</pre>';

echo '<hr>';
// 9. Определите, есть ли в массиве повторяющиеся элементы.
echo 'Task 9<br>';

$arr = array(1, 10, 2, 3, 4, 5, 6);
/**
 * @param array $arr
 * @return array
 */
function searchDuplicateInArr(array $arr):array
{
  $arrDup = array();
  $count = count($arr);
  $countForI = $count - 1;
  for ($i = 0; $i < $countForI; $i++) {
    for ($j = $i + 1; $j < $count; $j++) {
      if ($arr[$i] == $arr[$j]) {
        $arrDup[$i] = $arr[$i];
        $arrDup[$j] = $arr[$j];
      }
    }
  }
  return $arrDup;
}

$arrDup = searchDuplicateInArr($arr);
$count = count($arrDup);
echo 'В массиве:' . '<br>';
echo '<pre>';
print_r($arr);
echo '</pre>';
echo '<br>';

if ($count > 0) {
  echo 'Есть дубликаты в массиве';
} else {
  echo 'Нет дубликатов в массиве';
}
echo '<br>';
echo '<hr>';

// 10. Найти минимальное и максимальное среди 3 чисел
echo 'Task 10<br>';

$a = createNum();
$b = createNum();
$c = createNum();
/**
 * @param int ...$args
 * @return int
 */
function findMinNum(int ...$args):int
{
  $count = count($args);
  $countForI = $count - 1;
  $min = $args[0];
  for ($i = 0; $i < $countForI; $i++) {
    for ($j = $i + 1; $j < $count; $j++) {
      if ($min > $args[$j]) {
        $min = $args[$j];
      }
    }
  }
  return $min;
}

/**
 * @param int ...$args
 * @return int
 */
function findMaxNum(int ...$args):int
{
  $count = count($args);
  $countForI = $count - 1;
  $max = $args[0];
  for ($i = 0; $i < $countForI; $i++) {
    for ($j = $i + 1; $j < $count; $j++) {
      if ($max < $args[$j]) {
        $max = $args[$j];
      }
    }
  }
  return $max;
}

echo 'Среди чисел: ' . $a . ', ' . $b . ', ' . $c;
echo '<br>';
echo 'Минимальное значение = ' . findMinNum($a, $b, $c);
echo '<br>';
echo 'Максимальное значение = ' . findMaxNum($a, $b, $c);

echo '<hr>';

// 11. square
echo 'Task 11 square<br>';
$a = 3;
$b = 4;

echo 'Площадь ' . $a * $b . ' = ' . multiplication($a, $b);
echo '<br>';
echo '<hr>';

// 12. Теорема Пифагора
echo 'Task 12 Теорема Пифагора<br>';
$catet1 = 3;
$catet2 = 4;
$gipotenuza = 5;
/**
 * @param int $cat1
 * @param int $cat2
 * @return int
 */
function gipotenuza(int $cat1, int $cat2):int
{
  return sqrt(pow($cat1, 2) + pow($cat2, 2));
}

/**
 * @param int $cat1
 * @param int $gipotenuza
 * @return int
 */
function catet(int $cat1, int $gipotenuza):int
{
  return sqrt(pow($gipotenuza, 2) - pow($cat1, 2));
}

echo 'Катет 1 = ' . $catet1 . ', катет 2 = ' . $catet2 . ', а гипотенуза = ' . gipotenuza($catet1, $catet2);
echo '<br>';
echo 'Катет 1 = ' . $catet1 . ', гипотенуза = ' . $gipotenuza . ', а катет 2 = ' . catet($catet1, $gipotenuza);
echo '<hr>';

// 13. Найти периметр
echo 'Task 13 Периметр<br>';
/**
 * @param integer ...$args
 * @return integer
 */
function perimetr(int ...$args):integer
{
  $perimetr = 0;
  foreach ($args as $value) {
    $perimetr += $value;
  }
  return $perimetr;
}

$stor1 = 3;
$stor2 = 4;
$stor3 = 5;
$stor4 = 4;

echo 'Периметр фигуры = ' . perimetr($stor1, $stor2, $stor3);
echo '<hr>';

// 14. Найти дискриминант
echo 'Task 14 Найти дискриминант ax2 + bx + c = 0<br>';
/**
 * @param int $a
 * @param int $b
 * @param int $c
 * @return int
 */
function discriminant(int $a, int $b, int $c):int
{
  return pow($b, 2) - 4 * $a * $c;
}

$a = 2;
$b = 5;
$c = 4;

echo 'Дискриминант для ' . $a . 'x^2 + ' . $b . 'x + ' . $c . ' = ' . discriminant($a, $b, $c);
echo '<hr>';

// 15. Создать только четные числа до 100
echo 'Task 15 Создать только четные числа до 100<br>';

/**
 * @param int $min
 * @param int $max
 *
 * @return array
 */
function evenNum(int $min, int $max):array
{
  $arr = array();
  for ($i = $min; $i <= $max; $i++) {
    if ($i % 2 == 0) {
      $arr[] = $i;
    }
  }
  return $arr;
}

$min = 1;
$max = 100;
echo '<pre>';
print_r(evenNum($min, $max));
echo '</pre>';
echo '<hr>';

// 16. Создать не четные числа до 100
echo 'Task 16 Создать не четные числа до 100<br>';
/**
 * @param int $min
 * @param int $max
 * @return array
 */
function notEvenNum(int $min, int $max):array
{
  $arr = array();
  for ($i = $min; $i <= $max; $i++) {
    if ($i % 2 !== 0) {
      $arr[] = $i;
    }
  }
  return $arr;
}

echo '<pre>';
print_r(notEvenNum($min, $max));
echo '</pre>';
echo '<hr>';
