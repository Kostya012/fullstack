<?php

// Все задания делаем
//while
//do while
//for
//foreach
//    не используй готовые функции

// 1. Дан массив
//['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
//Развернуть этот массив в обратном направлении

$arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$arr[-2] = 'Kostya';
$arr[-1] = 'July';
// $keys = array_keys($arr);
$keys = array();
foreach ($arr as $key => $value) {
    $keys[] = $key;
}

$count = 0;
// while don't work, because !$keys[$count] = 0; 0-false
// while (true) {
//  if(!$keys[$count]) {
//    break;
//  }
//  $count++;
// }
// echo "count = $count";
// echo '<br>';

foreach ($keys as $value) {
    $count++;
}

// foreach
echo 'TASK 1<br>Foreach:<br>';
$i = 0;
if ($count > 0) {
    $i = $count - 1;
    foreach ($keys as $value) {
        echo $arr[$keys[$i]] . '-';
        $i--;
    }
} else {
    echo $arr[$keys[$i]];
}

//while
echo '<br>';
echo 'While:<br>';
$i = $count;
while ($i-- > 0) {
    echo $arr[$keys[$i]];
}

//do while
echo '<br>';
echo 'Do while:<br>';
$i = $count - 1;
do {
    echo $arr[$keys[$i]];
} while ($i-- > 0);

//for
echo '<br>';
echo 'For:<br>';
for ($i = $count - 1; $i >= 0; $i--) {
    echo $arr[$keys[$i]];
}
echo '<hr>';

//2. Дан массив
$arr = [44, 12, 11, 7, 1, 99, 43, 5, 69];
// Развернуть этот массив в обратном направлении

$count = 0;
foreach ($arr as $value) {
    $count++;
}

//while
echo 'TASK 2<br>While:<br>';
$i = $count;
while ($i-- > 0) {
    echo $arr[$i];
}

//do while
echo '<br>';
echo 'Do while:<br>';
$i = $count - 1;
do {
    echo $arr[$i];
} while ($i-- > 0);

//for
echo '<br>';
echo 'For:<br>';
for ($i = $count - 1; $i >= 0; $i--) {
    echo $arr[$i];
}

//foreach
echo '<br>';
echo 'Foreach:<br>';
$i = $count - 1;
foreach ($arr as $value) {
    echo $arr[$i] . '-';
    $i--;
}

echo '<hr>';

//3. Дана строка
// let str = 'Hi I am ALex'
// развенуть строку в обратном направлении.

$str = 'Hi I am ALex';

$count = 0;
while (true) {
    if (empty($str[$count])) {
        break;
    }
    $count++;
}

//while
echo 'TASK 3<br>While:<br>';
$i = $count;
while ($i-- > 0) {
    echo $str[$i];
}

//do while
echo '<br>';
echo 'Do while:<br>';
$i = $count - 1;
do {
    echo $str[$i];
} while ($i-- > 0);

//for
echo '<br>';
echo 'For:<br>';
for ($i = $count - 1; $i >= 0; $i--) {
    echo $str[$i];
}

//foreach
echo '<br>';
echo 'Foreach:<br>';
// $arr1 = str_split($str);
// or
$arr = array();
for ($i = 0; $i < $count; $i++) {
    $arr[] = $str[$i];
}

$i = $count - 1;
foreach ($arr as $value) {
    echo $str[$i];
    $i--;
}

echo '<hr>';

//4. Дана строка. готовую функцию toUpperCase() or
// tolowercase()
// let str = 'Hi I am ALex'
// сделать ее с с маленьких букв

$str = 'Hi I am ALex';
$str = strtolower($str);
echo 'TASK 4 strtolower($str):<br>';
echo $str;

echo '<hr>';

// 5. Дана строка
// let str = 'Hi I am ALex'
// сделать все буквы большие

$str = 'Hi I am ALex';
$str = strtoupper($str);
echo 'TASK 5 strtoupper($str):<br>';
echo $str;

echo '<hr>';

//6. Дана строка
// let str = 'Hi I am ALex'
// развернуть ее в обратном направлении
// по словам

$str = 'Hi I am ALex';

$count = 0;
while (true) {
    if (empty($str[$count])) {
        break;
    }
    $count++;
}

// $words = explode(' ', $str);
//or
$arr = array();
$word = $str[0];
for ($i = 1; $i < $count; $i++) {
    if ($str[$i] == ' ') {
        $arr[] = $word;
        $word = '';
    } elseif ($i == $count - 1) {
        $word .= $str[$i];
        $arr[] = $word;
    } else {
        $word .= $str[$i];
    }
}

$count = 0;
foreach ($arr as $value) {
    $count++;
}

//while
echo 'TASK 6<br>While:<br>';
$i = $count;
while ($i-- > 0) {
    echo $arr[$i];
}

//do while
echo '<br>';
echo 'Do while:<br>';
$i = $count - 1;
do {
    echo $arr[$i];
} while ($i-- > 0);

//for
echo '<br>';
echo 'For:<br>';
for ($i = $count - 1; $i >= 0; $i--) {
    echo $arr[$i];
}

//foreach
echo '<br>';
echo 'Foreach:<br>';
$i = $count - 1;
foreach ($arr as $value) {
    echo $arr[$i] . '-';
    $i--;
}

echo '<hr>';

//7. Дан массив
$arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
// сделать все буквы с маленькой
// strtolower
// strtoupper

$count = 0;
foreach ($arr as $value) {
    $arr[$count] = strtolower($value);
    $count++;
}

//while
echo 'TASK 7 (strtolower)<br>While:<br>';
$i = 0;
while ($i < $count) {
    echo $arr[$i] . ' ';
    $i++;
}

//do while
echo '<br>';
echo 'Do while:<br>';
$i = 0;
do {
    echo $arr[$i] . ' ';
    $i++;
} while ($i < $count);

//for
echo '<br>';
echo 'For:<br>';
for ($i = 0; $i < $count; $i++) {
    echo $arr[$i] . ' ';
}

//foreach
echo '<br>';
echo 'Foreach:<br>';
$i = 0;
foreach ($arr as $value) {
    echo $arr[$i] . '-';
    $i++;
}

echo '<hr>';

// 8. Дан массив
$arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
//сделать все буквы с большой

$count = 0;
foreach ($arr as $value) {
    $arr[$count] = strtoupper($value);
    $count++;
}

//while
echo 'TASK 8 (strtoupper)<br>While:<br>';
$i = 0;
while ($i < $count) {
    echo $arr[$i] . ' ';
    $i++;
}

//do while
echo '<br>';
echo 'Do while:<br>';
$i = 0;
do {
    echo $arr[$i] . ' ';
    $i++;
} while ($i < $count);

//for
echo '<br>';
echo 'For:<br>';
for ($i = 0; $i < $count; $i++) {
    echo $arr[$i] . ' ';
}

//foreach
echo '<br>';
echo 'Foreach:<br>';
$i = 0;
foreach ($arr as $value) {
    echo $arr[$i] . '-';
    $i++;
}

echo '<hr>';

// 9. Дано число
//let num = 1234678
//развернуть ее в обратном направлении

$num = 1234678;
$str = (string)$num;

$count = 0;
while (true) {
    if (empty($str[$count])) {
        break;
    }
    $count++;
}

//while
echo 'TASK 9<br>While:<br>';
$i = $count;
while ($i-- > 0) {
    echo $str[$i];
}

//do while
echo '<br>';
echo 'Do while:<br>';
$i = $count - 1;
do {
    echo $str[$i];
} while ($i-- > 0);

//for
echo '<br>';
echo 'For:<br>';
for ($i = $count - 1; $i >= 0; $i--) {
    echo $str[$i];
}

//foreach
echo '<br>';
echo 'Foreach:<br>';
// $arr1 = str_split($str);
// or
$arr = array();
for ($i = 0; $i < $count; $i++) {
    $arr[] = $str[$i];
}

$i = $count - 1;
foreach ($arr as $value) {
    echo $str[$i];
    $i--;
}

echo '<hr>';

// 10. Дан массив
$arr = [44, 12, 11, 7, 1, 99, 43, 5, 69];
//отсортируй его в порядке убывания

$count = 0;
foreach ($arr as $value) {
    $count++;
}

//while
echo 'TASK 10<br>While:<br>';
$i = 0;
$j = 1;
$arrSort = $arr;
$countFori = $count - 1;
while ($i < $countFori) {
    $j = $i + 1;
    while ($j <= $count) {
        $a = $arrSort[$i];
        $b = $arrSort[$j];
        if ($arrSort[$i] < $arrSort[$j]) {
            $arrSort[$i] = $b;
            $arrSort[$j] = $a;
        }
        $j++;
    }
    $i++;
}
print_r($arrSort);
echo '<br>';

//do while
echo 'Do while:<br>';
$i = 0;
$arrSort = $arr;
$countFori = $count - 1;
do {
    $j = $i + 1;
    do {
        $a = $arrSort[$i];
        $b = $arrSort[$j];
        if ($arrSort[$i] < $arrSort[$j]) {
            $arrSort[$i] = $b;
            $arrSort[$j] = $a;
        }
    } while ($j++ <= $count);
} while ($i++ < $countFori);
print_r($arrSort);
echo '<br>';

//for
echo 'For:<br>';
$arrSort = $arr;
$countFori = $count - 1;
for ($i = 0; $i < $countFori; $i++) {
    for ($j = $i + 1; $j <= $count; $j++) {
        $a = $arrSort[$i];
        $b = $arrSort[$j];
        if ($arrSort[$i] < $arrSort[$j]) {
            $arrSort[$i] = $b;
            $arrSort[$j] = $a;
        }
    }
}
print_r($arrSort);
echo '<br>';

//foreach
echo 'Foreach:<br>';
$arrSort = $arr;

foreach ($arrSort as $key => $value) {
    for ($j = $key + 1; $j <= $count; $j++) {
        $a = $arrSort[$key];
        $b = $arrSort[$j];
        if ($arrSort[$key] < $arrSort[$j]) {
            $arrSort[$key] = $b;
            $arrSort[$j] = $a;
        }
    }
}
print_r($arrSort);
echo '<br><hr>';
