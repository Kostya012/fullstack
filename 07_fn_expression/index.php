<?php
/**
 * @param array $arr
 * @return bool
 */
function dd(array $arr): bool
{
  echo '<pre>';
  print_r($arr);
  echo '</pre>';
  return true;
}

// Решаем двумя функциями
//- function expression
//- Стрелочной функцией

// 1. Найти минимальное и максимальное среди 3 чисел
echo 'Task 1 Найти минимальное и максимальное среди 3 чисел<br>';
$a = rand(1, 15);
$b = rand(1, 15);
$c = rand(1, 15);

//$findMinNum2 = function (...$args) {
//  $count = count($args);
//  $min = $args[0];
//  var_dump(array_filter($args, fn($v) => $v < $min));
//  echo '<br>';
//  var_dump(array_filter($args, function($v) use ($min) {
//   return  $v < $min;
//  }));
//  echo '<br>';
//
//  for($i=0;$i<$count;$i++){
//    if ($args[$i] < $min) {
//      $min = $args[$i];
//    }
//  }
//  return $min;
//};
//echo $findMinNum2($a, $b, $c);


$findMinNum = function (...$args) {
  $count = count($args);
  $min = $args[0];
  for ($i = 0; $i < $count; $i++) {
    if ($args[$i] < $min) {
      $min = $args[$i];
    }
  }
  return $min;
};
$findMaxNum = function (...$args) {
  $count = count($args);
  $max = $args[0];
  for ($i = 0; $i < $count; $i++) {
    if ($args[$i] > $max) {
      $max = $args[$i];
    }
  }
  return $max;
};

echo 'Минимальное число между переменными ' . $a . ', ' . $b . ', ' . $c . ' = ' . $findMinNum($a, $b, $c) . '<br>';
echo 'Максимальное число между переменными ' . $a . ', ' . $b . ', ' . $c . ' = ' . $findMaxNum($a, $b, $c) . '<br>';

$fnMinNum = fn($a, $b, $c) => min($a, $b, $c);
$fnMaxNum = fn($a, $b, $c) => max($a, $b, $c);

echo 'Минимальное число между переменными ' . $a . ', ' . $b . ', ' . $c . ' = ' . $fnMinNum($a, $b, $c) . '<br>';
echo 'Максимальное число между переменными ' . $a . ', ' . $b . ', ' . $c . ' = ' . $fnMaxNum($a, $b, $c) . '<br>';
echo '<hr>';

// 2. Найти площадь
echo 'Task 2 Найти площадь<br>';
$expression2 = function ($a, $b) {
  return $a * $b;
};
echo 'Площадь $a * $b = ' . $expression2($a, $b) . '<br>';

$fn2 = fn($a, $b) => $a * $b;
echo 'Площадь $a * $b = ' . $fn2($a, $b) . '<br>';
echo '<hr>';

// 3. Теорема Пифагора
echo 'Task 3 Теорема Пифагора<br>';
$catet1 = 3;
$catet2 = 4;
$gipotenuza = 5;

$getGipotenuza = function ($cat1, $cat2) {
  return sqrt(pow($cat1, 2) + pow($cat2, 2));
};
$getCatet = function ($cat1, $gipotenuza) {
  return sqrt(pow($gipotenuza, 2) - pow($cat1, 2));
};
echo 'Катет первый = $catet1, катет второй = $catet2, а гипотенуза = ' . $getGipotenuza($catet1, $catet2);
echo '<br>';
echo 'Катет первый = $catet1, гипотенуза = $gipotenuza, а катет второй = ' . $getCatet($catet1, $gipotenuza);
echo '<hr>';

// 4. Найти периметр
echo 'Task 4 Найти периметр<br>';

$findPerimetr = function (...$args) {
  $perimetr = 0;
  foreach ($args as $value) {
    $perimetr += $value;
  }
  return $perimetr;
};
echo 'Периметр фигуры = ' . $findPerimetr($a, $b, $c);
echo '<br>';

$findPerimetr2 = fn(...$args) => array_sum($args);
echo 'Периметр фигуры = ' . $findPerimetr2($a, $b, $c);
echo '<hr>';

// 5. Найти дискриминант
echo 'Task 5 Найти дискриминант<br>';

$discriminant = function ($a, $b, $c) {
  return pow($b, 2) - 4 * $a * $c;
};

echo 'Дискриминант для $a x^2 + $b x + $c = ' . $discriminant($a, $b, $c);
echo '<br>';

$discriminant2 = fn($a, $b, $c) => pow($b, 2) - 4 * $a * $c;

echo 'Дискриминант для $a x^2 + $b x + $c = ' . $discriminant2($a, $b, $c);
echo '<hr>';

// 6. Создать только четные числа до 100
echo 'Task 6 Создать только четные числа до 100<br>';

/**
 * @param int $min
 * @param int $max
 *
 * @return array
 */
$createEvenNum = function (int $min, int $max): array {
  $arr = array();
  for ($i = $min; $i <= $max; $i++) {
    if ($i % 2 == 0) {
      $arr[] = $i;
    }
  }
  return $arr;
};

$createArrNum = function ($min, $max) {
  $arr = array();
  for ($i = $min; $i <= $max; $i++) {
    $arr[] = $i;
  }
  return $arr;
};
$min = 0;
$max = 100;

$arrEven = $createEvenNum($min, $max);
echo 'Массив чётных чисел до 100:<br>';
dd($arrEven);

$arr = $createArrNum($min, $max);
echo 'Массив чётных чисел до 100:<br>';
$arrEvenFn = array_filter($arr, fn($item) => $item % 2 == 0);
dd($arrEvenFn);

echo '<hr>';

// 7. Создать нечетные числа до 100
echo 'Task 7 Создать нечетные числа до 100<br>';

/**
 * @param int $min
 * @param int $max
 *
 * @return array
 */
$createNotEvenNum = function (int $min, int $max): array {
  $arr = array();
  for ($i = $min; $i <= $max; $i++) {
    if ($i % 2 == 1) {
      $arr[] = $i;
    }
  }
  return $arr;
};
$min = 0;
$max = 100;

$arrNotEven = $createNotEvenNum($min, $max);
echo 'Массив нечётных чисел до 100:<br>';
dd($arrNotEven);

echo 'Массив нечётных чисел до 100:<br>';
$arrNotEvenFn = array_filter($arr, fn($item) => $item % 2 == 1);
dd($arrNotEvenFn);
echo '<hr>';

// 8. Определите, есть ли в массиве повторяющиеся элементы.
echo 'Task 8 Определите, есть ли в массиве повторяющиеся элементы.<br>';

$searchDuplicateInArr = function ($arr) {
  $arrDup = array();
  $count = count($arr);
  $countForI = $count - 1;
  for ($i = 0; $i < $countForI; $i++) {
    for ($j = $i + 1; $j < $count; $j++) {
      if ($arr[$i] == $arr[$j]) {
        $arrDup[$i] = $arr[$i];
        $arrDup[$j] = $arr[$j];
      }
    }
  }
  return $arrDup;
};

$arrDup = $searchDuplicateInArr($arr);
$count = count($arrDup);
echo 'В массиве:' . '<br>';

dd($arr);

echo '<br>';

if ($count > 0) {
  echo 'Есть дубликаты в массиве';
} else {
  echo 'Нет дубликатов в массиве';
}

/**
 * @param array $arr
 *
 * @return array
 */
$searchDuplicateInArrFn = array_filter(array_count_values($arr), fn($v, $key) => $v > 1, ARRAY_FILTER_USE_BOTH);

$count = count($searchDuplicateInArrFn);
echo '<br>';

if ($count > 0) {
  echo 'Есть дубликаты в массиве';
} else {
  echo 'Нет дубликатов в массиве';
}

echo '<hr>';

// 9. Сортировка из прошлого задания
echo 'Task 9 Сортировка из прошлого задания<br>';

$sortArr = function ($arr, $by = 'asc') {
  $newArr = $arr;
  $count = 0;
  foreach ($arr as $value) {
    $count++;
  }
  $countForI = $count - 1;
  if ($by == 'asc') {
    for ($i = 0; $i < $count; $i++) {
      for ($j = $count - 1; $j > $i; $j--) {
        if ($newArr[$i] > $newArr[$j]) {
          $tempArr = $newArr[$i];
          $newArr[$i] = $newArr[$j];
          $newArr[$j] = $tempArr;
        }
      }
    }
  } elseif ($by == 'desc') {
    for ($i = 0; $i < $count; $i++) {
      for ($j = $count - 1; $j > $i; $j--) {
        if ($newArr[$i] < $newArr[$j]) {
          $tempArr = $newArr[$i];
          $newArr[$i] = $newArr[$j];
          $newArr[$j] = $tempArr;
        }
      }
    }
  } else {
    return 'Не верно переданы параметры';
  }
  return $newArr;
};
$arr = array(1, 3, 6, 6, 7, 3, 8, 5);
$newArr = $sortArr($arr, 'desc');
// $newArr = sortArr($arr, 'asc');
echo 'Отсортированный массив:<br>';
dd($newArr);

echo 'Сортируем массив fn:<br>';
dd($arr);
/**
 * @param array $arr
 *
 * @return array
 */
$sortArrFn = fn(&$arr, $a = 'asc') => ($a == 'desc') ? rsort($arr) : sort($arr);

$arrUp = $sortArrFn($arr, 'asc'); // если успешно вернёт 1

echo 'Отсортированный массив по возрастанию:<br>';
dd($arr);

$arrDown = $sortArrFn($arr, 'desc'); // если успешно вернёт 1

echo 'Отсортированный массив по убыванию:<br>';
dd($arr);
echo '<hr>';