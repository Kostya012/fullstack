<?php
// Task 1
$a = 42;
$b = 55;

echo ($a >= $b) ? 'number ' . $a . ' >= ' . $b : 'number ' . $a . ' < ' . $b;
echo '<br>';

// Task 2
$a = rand(5, 15);
$b = rand(5, 15);

echo ($a >= $b) ? 'number ' . $a . ' >= ' . $b : 'number ' . $a . ' < ' . $b;
echo '<br>';

// Task 3
$surname = 'Shevtsov';
$name = 'Kostiantyn';
$fathname = 'Olegovich';

$snf = $surname . ' ' . $name[0] . '. ' . $fathname[0] . '. ';
echo $snf;
echo '<br>';

// Task 4
$a = rand(1, 99999);
$b = 7;
$countB = substr_count($a, $b);
echo 'Number ' . $b . ' is set ' . $countB . ' in ' . $a;
echo '<br>';

// Task 5 variable
$a = 3;
print ($a);
echo '<br>';

$a = 10;
$b = 2;
echo 'Сумма =' . ($a + $b) . '<br>';
echo 'Разность =' . ($a - $b) . '<br>';
echo 'Умножение =' . ($a * $b) . '<br>';
echo 'Деление =' . ($a / $b) . '<br>';

$c = 15;
$d = 2;
$result = $c + $d;
echo 'Сумма = ' . $result . '<br>';
$a = 10;
$b = 2;
$c = 5;
echo 'Сумма =' . ($a + $b + $c) . '<br>';

$a = 17;
$b = 10;
$c = $a - $b;
$d = rand(5, 15);

// Task 6
$result = $c + $d;
echo 'Сумма = ' . $result . '<br>';

// Task 7
$text = 'Привет, Мир!';
echo $text . '<br>';

$text1 = 'Привет, ';
$text2 = 'Мир!';
echo $text1 . $text2 . '<br>';

$sec = 60;
echo 'Секунд в часе = ' . ($sec * 60) . '<br>';
echo 'Секунд в сутках = ' . ($sec * 60 * 24) . '<br>';
echo 'Секунд в неделе = ' . ($sec * 60 * 24 * 7) . '<br>';
echo 'Секунд в неделе = ' . ($sec * 60 * 24 * 30) . '<br>';

// Task 8
$var = 1;
$var += 12;
$var -= 14;
$var *= 5;
$var /= 7;
$var++;
$var--;
echo $var . '<br>';

// Task 1**
date_default_timezone_set('Europe/Kiev');
$hour = date('h');
$minute = date('i');
$sec = date('s');
echo $hour . ':' . $minute . ':' . $sec . '<br>';

// Task 2**
$text = 'Я';
$text .= ' хочу';
$text .= ' знать';
$text .= ' PHP!';
echo $text;
echo '<br>';

// Task 3**
$foo = 'bar';
$bar = 10;
echo $$foo;
echo '<br>';

// Task 4**
$a = 2;
$b = 4;
echo ($a++) + $b;// 6
echo '<br>';
echo $a + (++$b); // 8
echo '<br>';
echo (++$a) + ($b++); // 9
echo '<br>';

// Task 5**

$a = 'asd';
echo isset($a) ? 'переменная существует' : 'переменная не существует';
echo '<br>';
echo (gettype($a) === 'string') ? 'переменная строка' : 'переменная не строка';
echo '<br>';
$a = null;
echo is_null($a) ? 'переменная  null' : 'переменная не null';
echo '<br>';
echo empty($a) ? '$a или 0, или пусто, или вообще не определена' : 'переменная не пустая';
echo '<br>';
$a = 112;
echo is_integer($a) ? 'переменная целочисленное число' : 'переменная не целочисленное число';
echo '<br>';
// or
echo is_int($a) ? 'переменная целочисленное число' : 'переменная не целочисленное число';
echo '<br>';
$a = 1e7;
echo is_double($a) ? 'переменная является числом с плавающей точкой' : 'переменная не является числом с плавающей точкой';
echo '<br>';
// or
echo is_float($a) ? 'переменная является числом с плавающей точкой' : 'переменная не является числом с плавающей точкой';
echo '<br>';
$a = 'asd';
echo is_string($a) ? 'переменная строка' : 'переменная не строка';
echo '<br>';
$a = '05';
echo is_numeric($a) ? 'переменная является числом или строкой, содержащей число' : 'переменная не является числом или строкой, содержащей число';
echo '<br>';
$a = false;
echo is_bool($a) ? 'переменная булевая' : 'переменная не булевая';
echo '<br>';
$a = false;
echo is_scalar($a) ? 'переменная скалярная (int, float, string или bool)' : 'переменная не скалярная (null, array, object или resource)';
echo '<br>';
$a = null;
echo is_null($a) ? 'переменная имеет значение null' : 'переменная не имеет значение null';
echo '<br>';
$a = array();
echo is_array($a) ? 'переменная массив (array)' : 'переменная не массив (array)';
echo '<br>';
$a = new stdClass();
echo is_object($a) ? 'переменная объект' : 'переменная не объект';
echo '<br>';

// Task 1***
$a = 2;
$b = 5;
$c = $a + $b;
echo 'сумма ' . $a . ' + ' . $b . ' = ' . $c . '<br>';
echo 'произведение ' . $a . ' * ' . $b . ' = ' . $a * $b . '<br>';

// Task 2***
echo pow($a, 2) + pow($b, 2);
echo '<br>';
// or
echo $a ** 2 + $b ** 2;
echo '<br>';
// or с учётом, если переменная с минусом
echo ($a) ** 2 + ($b) ** 2;
echo '<br>';

// Task 3***
echo ($a + $b + $c) / 3;
echo '<br>';

// Task 4***
$x = 3;
$y = 4;
$z = 5;
echo ($x + 1) - 2 * ($z - 2 * $x + $y);
echo '<br>';

// Task 5***
$a = 25;
echo 'Остаток от деления числа ' . $a . ' на 3 = ' . $a % 3;
echo '<br>';
echo 'Остаток от деления числа ' . $a . ' на 5 = ' . $a % 5;
echo '<br>';
echo 'Увеличение числа ' . $a . ' на 30% = ' . $a * 1.3;
echo '<br>';
echo 'Увеличение числа ' . $a . ' на 120% = ' . $a * 2.2;
echo '<br>';

// Task 6***
$a = 100;
$b = 150;
echo 'сумма 40% от $a и 84% от $b = ' . ($a * 0.4 + $b * 0.84);
echo '<br>';
$num = rand(100, 999);
echo 'Сумма чисел числа $num = ' . array_sum(str_split($num, 1));
echo '<br>';

// Task 7***
$num = $num2 = (string)rand(100, 999);

echo 'Меняем у числа ' . $num . ' среднее число на ноль';
$num[1] = 0;
echo '<br>';
echo 'получается: ' . $num;
echo '<br>';
echo 'Реверс порядка числа: ' . strrev($num2);
echo '<br>';

// Task 8***
echo 'Проверка на чётность числа ' . $num2 . ' - это ';
echo ($num2 % 2) ? 'число нечетное' : 'число четное';
