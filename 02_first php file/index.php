<?php
$name = 'Kostiantyn';
echo 'Ваше имя: ' . $name . ' (from echo)<br>';
print ($name . ' (from print)<br>');
print_r($name);
echo ' (from print_r)<br>';

$age = 33;
echo 'Ваш возраст: ' . $age . ' (from echo)<br>';
print ($age . '(from print)<br>');
print_r($age);
echo ' ( from print_r)<br>';

$pi = pi();
echo 'Число Pi: ' . $pi . ' (from echo)<br>';
print ($pi . '(from print)<br>');
print_r($pi);
echo ' ( from print_r)<br>';
echo '<hr>';

$arr = array('alex', 'vova', 'tolya');
//or
$ar = ['alex', 'vova', 'tolya'];

$arr2 = ['alex', 'vova', 'tolya', ['kostya', 'olya']];
$arr3 = ['alex', 'vova', 'tolya', ['kostya', 'olya', ['gosha', 'mila']]];
$arr4 = [['alex', 'vova', 'tolya'], ['kostya', 'olya'], ['gosha', 'mila']];

/**
 * @param string|array $arg
 *
 * @return boolean
 */
function outputEcho(string|array $arg): bool
{
  if (gettype($arg) === 'array') {
    foreach ($arg as $value) {
      if (gettype($value) === 'array') {
        foreach ($value as $val) {
          if (gettype($val) === 'array') {
            foreach ($val as $v) {
              echo $v . '<br>';
            }
          } else {
            echo $val . '<br>';
          }
        }
      } else {
        echo $value . '<br>';
      }
    }
  } else {
    echo $arg . '<br>';
  }
  return true;
}

/**
 * @param string|array $arg
 *
 * @return boolean
 */
function outputPrint(string|array $arg): bool
{
  if (gettype($arg) === 'array') {
    foreach ($arg as $value) {
      if (gettype($value) === 'array') {
        foreach ($value as $val) {
          if (gettype($val) === 'array') {
            foreach ($val as $v) {
              print ($v . '<br>');
            }
          } else {
            print ($val . '<br>');
          }
        }
      } else {
        print ($value . '<br>');
      }
    }
  } else {
    print ($arg . '<br>');
  }
  return true;
}

echo 'output echo:<br>';
outputEcho($arr);
echo '<hr>';
outputEcho($arr2);
echo '<hr>';
outputEcho($arr3);
echo '<hr>';
outputEcho($arr4);
echo '<hr>';

echo 'output print:<br>';
outputPrint($arr);
echo '<hr>';
outputPrint($arr2);
echo '<hr>';
outputPrint($arr3);
echo '<hr>';
outputPrint($arr4);
echo '<hr>';

echo 'output print_r:<br>';
print_r($arr);
echo '<hr>';
print_r($arr2);
echo '<hr>';
print_r($arr3);
echo '<hr>';
print_r($arr4);
