<?php
//1. Cоздать форму оформить заказ
//  a. имя
//  b. телефон
//  c. тип доставки
//  d. адрес доставки
//  e. сессия получить список id товаров и передать в форму
session_start();
$_SESSION['id'] = '2525-3825-2388';
/**
 * @param string $value
 * @return string
 */
function clean(string $value = ''):string
{
  $value = strip_tags($value);
  $value = htmlspecialchars($value);
  return $value;
}

/**
 * @return string
 */
function genToken():string
{
  $token = md5(microtime() . 'salt' . time());
  return $token;
}

if (isset($_POST['send'])) {
  $token = clean($_POST['token']);
  if ($token === $_SESSION['token']) {
    $idProduct = clean($_POST['idProduct']);
    $idProduct = explode('-', $idProduct);
    $name = clean($_POST['name']);
    $phone = clean($_POST['phone']);
    $delivery = clean($_POST['delivery']);
    $delAddress = clean($_POST['delAddress']);
  } else {
    echo 'token error';
    exit();
  }
} else {
  $_SESSION['token'] = genToken();
}
?>
<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Home work ***</title>
  <style>
      div {
          margin-bottom: 10px;
          position: relative;
      }

      input[type="tel"] {
          width: 100px;
      }

      input + span {
          padding-right: 30px;
      }

      input:invalid + span:after {
          position: absolute;
          content: '✖';
          padding-left: 5px;
          color: #8b0000;
      }

      input:valid + span:after {
          position: absolute;
          content: '✓';
          padding-left: 5px;
          color: #009000;
      }

      textarea:invalid {
          border: 2px dashed red;
      }

      textarea:valid {
          border: 2px solid lime;
      }
  </style>
</head>
<body>
<h2>Exercise 1 checkout</h2>
<?php
if (isset($idProduct) && gettype($idProduct) === 'array' && count($idProduct) > 0) {
  $idProduct = implode(', ', $idProduct);
  echo 'ID products: ' . $idProduct . '<br>';
  echo 'Your name: ' . $name . '<br>';
  echo 'Your phone: ' . $phone . '<br>';
  echo 'Type delivery: ' . $delivery . '<br>';
  echo 'Delivery address: ' . $delAddress . '<br>';
}
?>
<form name="checkout" action="<?php echo $_SERVER['SCRIPT_NAME']; ?>" method="post">
  <input type="hidden" name="idProduct" value="<?php echo $_SESSION['id']; ?>">
  <input type="hidden" name="token" value="<?php echo $_SESSION['token']; ?>">
  <div class="form-group">
    <label for="name">Your name:</label><br>
    <input type="text" name="name" id="name" placeholder="Enter your name" required minlength="2" maxlength="11">
    <span class="validity"></span>
    <span>(A..z, 0..9, min 2 chars)</span><br>
  </div>
  <div class="form-group">
    <label for="phone">Enter a telephone number (in the form xxx-xxx-xxxx):</label><br>
    <span>+38</span><input type="tel" name="phone" id="phone" required
                           pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}" placeholder="Enter your phone">
    <span class="validity"></span><br>
  </div>
  <div class="form-group">
    <label for="delivery">Type of delivery:</label><br>
    <select name="delivery" size="1">
      <option value=1 selected>Ukrposhta</option>
      <option value=2>Nova poshta</option>
      <option value=3>Justin</option>
      <option value=4>Express</option>
    </select>
  </div>
  <div class="form-group">
    <label for="delAddress">Delivery address (A..z, 0..9, min 7 chars)</label><br>
    <textarea name="delAddress" id="delAddress" rows="7" cols="50" placeholder="Enter delivery address" required
              minlength="7" maxlength="50"></textarea>
    <span class="validity"></span><br>
  </div>
  <input type=submit name="send" value="Done">
  <input type="reset">
</form>
<hr>
</body>
</html>
