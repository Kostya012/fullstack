<?php
// Создать функцию по нахождению числа в степени
echo 'Task 1 Создать функцию по нахождению числа в степени<br>';
/**
 *  @param int $num
 *  @param int $stepen
 *  @return int
 */
function numInStepen(int $num, int $stepen): int
{
  if ($stepen == 0) {
    return 1;
  }
  $n = 1;
  for ($i = 1; $i <= $stepen; $i++) {
    $n *= $num;
  }
  return $n;
}

$num = 5;
$stepen = 2;
echo 'Число $num в степени ' . $stepen . ' = ' . numInStepen($num, $stepen);
echo '<hr>';

// написать функцию сортировки. Функция принимает массив случайных чисел и сортирует их по порядку. По дефолту функция сортирует в порядке возрастания. Но если передать второй параметр то функция будет сортировать по убыванию.
//sort(arr)
//sort(arr, 'asc')
//sort(arr, 'desc')
/**
 *  @param array $arr
 *  @param string $by
 *  @return string|array
 */
function sortArr(array $arr, string $by = 'asc'): string|array
{
  $newArr = $arr;
  $count = 0;
  foreach ($arr as $value) {
    $count++;
  }
  $countForI = $count - 1;
  if ($by == 'asc') {
    for ($i = 0; $i < $count; $i++) {
      for ($j = $count - 1; $j > $i; $j--) {
        if ($newArr[$i] > $newArr[$j]) {
          $tempArr = $newArr[$i];
          $newArr[$i] = $newArr[$j];
          $newArr[$j] = $tempArr;
        }
      }
    }
  } elseif ($by == 'desc') {
    for ($i = 0; $i < $count; $i++) {
      for ($j = $count - 1; $j > $i; $j--) {
        if ($newArr[$i] < $newArr[$j]) {
          $tempArr = $newArr[$i];
          $newArr[$i] = $newArr[$j];
          $newArr[$j] = $tempArr;
        }
      }
    }
  } else {
    return 'Не верно переданы параметры';
  }
  return $newArr;
}

$arr = array(1, 3, 6, 6, 7, 3, 8, 5);
$newArr = sortArr($arr, 'desc');
// $newArr = sortArr($arr, 'asc');
echo '<pre>';
print_r($newArr);
echo '</pre>';
echo '<hr>';

// написать функцию поиска в массиве. функция будет принимать два параметра. Первый массив, второй поисковое число.
$find = 9;
/**
 * @param array   $arr
 * @param int   $find
 * @return string
 */
function search(array $arr, int $find):string
{
  $count = 0;
  foreach ($arr as $value) {
    $count++;
  }
  foreach ($arr as $value) {
    if ($find == $value) {
      return 'Число ' . $find . ' есть в массиве';
    }
  }
  return 'Число ' . $find . ' нету в массиве';
}

echo search($arr, $find);
