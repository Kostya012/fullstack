<?php
// ДЗ:
// 1) Написать программу, которая выводит простые числа, т.е. делящиеся без
// остатка только на себя и на 1.

$primeNum = array();
$res = true;

for ($i = 2; $i < 100; $i++) {
  for ($j = 2; $j < $i; $j++) {
    if (($i % $j) == 0) {
      $res = false;
    }
  }
  if ($res) $primeNum[] = $i;
  else $res = true;
}
foreach ($primeNum as $value) echo $value . '<br>';

//2) Сгенерируйте 100 раз новое число и выведите на экран количество четных чисел из этих 100.

$minNum = 3;
$maxNum = $minNum + 100;
$countEven = 0;

for ($i = $minNum; $i < $maxNum; $i++) {
  if (($i % 2) == 0) {
    $countEven++;
  }
}
echo 'Number of even numbers from ' . $minNum . ' to ' . $maxNum . ' = ' . $countEven . ' <br>';

// 3) Сгенерируйте 100 раз число от 1 до 5 и выведите на экран сколько раз сгенерировались эти числа (1, 2, 3, 4 и 5).

$maxNum = 100;
$count1 = 0;
$count2 = 0;
$count3 = 0;
$count4 = 0;
$count5 = 0;

for ($i = 0; $i < $maxNum; $i++) {
  $num = rand(1, 5);
  if ($num == 1) {
    $count1++;
  } elseif ($num == 2) {
    $count2++;
  } elseif ($num == 3) {
    $count3++;
  } elseif ($num == 4) {
    $count4++;
  } else {
    $count5++;
  }
}
echo 'Number 1 was generated ' . $count1 . ' times<br>';
echo 'Number 2 was generated ' . $count2 . ' times<br>';
echo 'Number 3 was generated ' . $count3 . ' times<br>';
echo 'Number 4 was generated ' . $count4 . ' times<br>';
echo 'Number 5 was generated ' . $count5 . ' times<br>';

// 4) Используя условия и циклы сделать таблицу в 5 колонок и 3 строки (5x3), отметить разными цветами часть ячеек.

echo '<table border="1"';
for ($a = 0; $a < 3; $a++) {
  echo '<tr>';
  for ($b = 0; $b < 5; $b++) {
    if ($b == 0) {
      echo '<td style=background-color:rgb(85,51,51);>&nbps</td>';
    } elseif ($b == 1) {
      echo '<td style=background-color:rgb(85,102,102);>&nbps</td>';
    } elseif ($b == 2) {
      echo '<td style=background-color:rgb(85,153,153);>&nbps</td>';
    } elseif ($b == 3) {
      echo '<td style=background-color:rgb(85,204,204);>&nbps</td>';
    } else {
      echo '<td style=background-color:rgb(85,255,255);>&nbps</td>';
    }
  }
}
echo '</tr>';
echo '</table>';

// 1. В переменной month лежит какое-то число из интервала от 1 до 12. Определите в какую пору года попадает этот месяц (зима, лето, весна, осень).

$month = rand(1, 12);
if ($month <= 2 || $month == 12) {
  echo $month . ' this Winter<br>';
} elseif ($month <= 5) {
  echo $month . ' this Spring<br>';
} elseif ($month <= 8) {
  echo $month . ' this Summer<br>';
} elseif ($month <= 11) {
  echo $month . ' this Autumn<br>';
}

// 2. Дана строка, состоящая из символов, например, 'abcde'. Проверьте, что первым символом этой строки является буква 'a'. Если это так - выведите 'да', в противном случае выведите 'нет'.

$str = 'abcde';
echo ($str[0] == 'a') ? 'да' : 'нет';
echo '<br>';

// 3. Дана строка с цифрами, например, '12345'. Проверьте, что первым символом этой строки является цифра 1, 2 или 3. Если это так - выведите 'да', в противном случае выведите 'нет'.

$str = '12345';
for ($i = 1; $i <= 3; $i++) {
  if ($str[0] == $i) {
    echo 'да';
    echo '<br>';
    break;
  }
}

// 4. Если переменная test равна true, то выведите 'Верно', иначе выведите 'Неверно'. Проверьте работу скрипта при test, равном true, false. Напишите два варианта скрипта - тернарка и if else.

$test = true;
echo ($test == true) ? 'Верно' : 'Неверно';
echo '<br>';

if ($test == true) {
  echo 'Верно';
} else {
  echo 'Неверно';
}
echo '<br>';

// 5. Дано Два массива рус и англ ['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс'] Если переменная lang = ru вывести массив на русском языке, а если en то вывести на английском языке. Сделат через if else и через тернарку.

$lang = 'en';
$arrRu = ['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс'];
$arrEn = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'];

if ($lang == 'en') {
  foreach ($arrEn as $val) {
    echo $val;
  }
} else {
  foreach ($arrRu as $val) {
    echo $val;
  }
}
echo '<br>';

$arrLeng = ($lang == 'en') ? $arrEn : $arrRu;
foreach ($arrLeng as $val) {
  echo $val;
}
echo '<br>';

// or
/**
 * @param array $arr
 * @return string
 */
function getDays(array $arr):string
{
  $days = '';
  foreach ($arr as $val) {
    $days .= $val . " ";
  }
  return $days;
}

echo ($lang == 'en') ? getDays($arrEn) : getDays($arrRu);
echo '<br>';

// 6. В переменной cloсk лежит число от 0 до 59 – это минуты. Определите в какую четверть часа попадает это число (в первую, вторую, третью или четвертую). тернарка и if else.

$cloсk = rand(0, 59);

echo ($cloсk < 15) ? $cloсk . ' this 1 parth' : ($cloсk < 30) ? $cloсk . ' this 2 parth' : ($cloсk < 45) ? $cloсk . ' this 4 parth' : $cloсk . ' this 4 parth';

echo '<br>';

if ($cloсk < 15) {
  echo $cloсk . ' this 1 parth<br>';
} elseif ($cloсk < 30) {
  echo $cloсk . ' this 2 parth<br>';
} elseif ($cloсk < 45) {
  echo $cloсk . ' this 3 parth<br>';
} elseif ($cloсk < 60) {
  echo $cloсk . ' this 4 parth<br>';
}
