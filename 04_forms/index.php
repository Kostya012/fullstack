<?php
//echo $_SERVER['SERVER_PROTOCOL'];
//echo '<br>';
//echo $_SERVER['DOCUMENT_ROOT'];

//1.создать форму оставить отзыв
//  a. поле имя
//  b. поле комментарий
//2.создать форму добавить товар в корзину
//  a. id товара
//  b. количество
//3.обратная связь перезвонить клиенту
//  a. имя
//  b. телефон
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home work**</title>
    <style>
        div {
            margin-bottom: 10px;
            position: relative;
        }

        #phone[type="tel"] {
            width: 100px;
        }

        #phone + span {
            padding-right: 30px;
        }

        #phone:invalid+span:after {
            position: absolute; content: '✖';
            padding-left: 5px;
            color: #8b0000;
        }

        #phone:valid+span:after {
            position: absolute;
            content: '✓';
            padding-left: 5px;
            color: #009000;
        }
    </style>
</head>
<body>
<h2>Exercise 1 feedback</h2>
<form name="feedback" action="<?php echo $_SERVER['SCRIPT_NAME'];?>" method="post">
    <?php
    if (isset($_REQUEST['doGo'])) {
        $name = htmlspecialchars($_REQUEST['name']);
        $comment = htmlspecialchars($_REQUEST['comment']);
        echo 'Your name: ' . $name . '<br>';
        echo 'Your comment: ' . $comment . '<br>';
    }
    ?>
    <div class="form-group">
        <label for="name">Your name:</label><br>
        <input type="text" name="name" id="name" placeholder="Enter your name" >
        <span>(A..z, 0..9)</span><br>
    </div>
    <div class="form-group">
        <label for="comment">Your comment</label><br>
        <textarea name="comment" id="comment" rows="7" cols="80" placeholder="Enter your comment"></textarea><br>
    </div>
    <input type=submit name="doGo" value="Go!">
</form>
<hr>
<h2>Exercise 2 Add to basket</h2>
<form name="addToBasket" action="<?php echo $_SERVER['SCRIPT_NAME'];?>" method="post">
    <?php
    if (isset($_REQUEST['doGoBasket'])) {
        $id = htmlspecialchars($_REQUEST['id']);
        $count = htmlspecialchars($_REQUEST['count']);
        echo 'ID of the product: ' . $id . '<br>';
        echo 'Quantity of product: ' . $count . '<br>';
    }
    ?>
    <div class="form-group">
        <label for="id">ID of the product</label><br>
        <input type="number" name="id" id="id" placeholder="Enter ID of the product" >
        <span>(0..9)</span><br>
    </div>
    <div class="form-group">
        <label for="count">Quantity of product</label><br>
        <input type="number" name="count" id="count" min="1" max="100" value=1 ><br>
    </div>
    <input type=submit name="doGoBasket" value="Add to Basket">
</form>
<hr>
<h2>Exercise 3 Callback</h2>
<form name="callback" action="<?php echo $_SERVER['SCRIPT_NAME'];?>" method="post">
    <?php
    if (isset($_REQUEST['doGoCallback'])) {
        $name2 = htmlspecialchars($_REQUEST['name2']);
        $phone = htmlspecialchars($_REQUEST['phone']);
        echo 'Your name: ' . $name2 . '<br>';
        echo 'Your phone: ' . $phone . '<br>';
    }
    ?>
    <div class="form-group">
        <label for="name2">Your name:</label><br>
        <input type="text" name="name2" id="name2" placeholder="Enter your name" >
        <span>(A..z, 0..9)</span><br>
    </div>
    <div class="form-group">
        <label for="phone">Enter a telephone number (in the form xxx-xxx-xxxx):</label><br>
        <span>+38</span><input type="tel" name="phone" id="phone" required
               pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}" placeholder="Enter your phone" >
        <span class="validity"></span><br>
    </div>
    <input type=submit name="doGoCallback" value="Done">
</form>
<hr>
</body>
</html>
