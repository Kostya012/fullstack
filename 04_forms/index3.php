<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home work - upload file ***</title>
</head>
<body>
<h2>Upload file</h2>
<?php
$max_image_width = 1280;
$max_image_height = 960;
$max_image_size = 960 * 1280;
$valid_types = array('pdf', 'xls', 'xlsx', 'doc', 'docx', 'png', 'jpg');
if (isset($_FILES['userfile'])) {
    if (is_uploaded_file($_FILES['userfile']['tmp_name'])) {
        $filename = basename($_FILES['userfile']['name']);
        $ext = substr($_FILES['userfile']['name'], 1 + strrpos($_FILES['userfile']['name'], '.'));
        echo 'type ' . $ext . '<br>';
        // strpos -- Возвращает позицию первого вхождения подстроки
        // получим массив свойств файла
        if (!in_array($ext, $valid_types)) {
            echo 'Error: Invalid file type.';
            exit();
        }
        if ($ext === 'png' || $ext === 'jpg') {
            $size = getimagesize($_FILES['userfile']['tmp_name']);
            if (filesize($_FILES['userfile']['tmp_name']) > $max_image_size) {
                echo 'Error: File size > ' . $max_image_size;
            } elseif (($size) && ($size[0] < $max_image_width) && ($size[1] < $max_image_height)) {
                $uploaddir = $_SERVER['DOCUMENT_ROOT'] . '/uploadfiles/';
                $uploadfile = $filename;
                move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile);
                echo '<img src="' . $uploadfile . '" alt="' . $filename . '" title="' . $filename . '" />';
            } else {
                echo 'Error: invalid image properties.';
            }
        } else {
            $size = filesize($_FILES['userfile']['tmp_name']);
        }
        echo 'size ' . $size . '<br>';
    } else {
        echo 'Error: empty file.';
    }
} else {
    echo '
        <form enctype="multipart/form-data" method="post">
        Send this file: <input name="userfile" type="file" accept=".pdf, .xls, .xlsx, .doc, .docx, image/png, .jpg">
        <input type="submit" value="Send File">
        </form>';
}
?>
</body>
</html>
